#!usr/bin/env node

import {getArgsHelpers} from "./helpers/get-args.helpers.js";
import {getError, getHelp, getSuccess, printWeather} from "./services/log.services.js";
import {saveKeyValue, STORAGE_KEYS} from "./services/storage.service.js";
import {getWeather} from "./services/get-weather-api.service.js";

const setKeyValue = async (key = STORAGE_KEYS.token || STORAGE_KEYS.city, value) => {
    if (!value.length) {
        getError("Не указано значение")
        return;
    }
    try {
        if(key === STORAGE_KEYS.token) {
            await saveKeyValue(key, value)
            getSuccess("Токен установлен")
        }
        if (key === STORAGE_KEYS.city) {
            await saveKeyValue(key, value)
            getSuccess("Город установлен")
        }
    } catch (e) {
        getError(e.message)
    }
}

const getForCast = async () => {
    try {
        const weather = await getWeather()
        printWeather(weather)
    } catch (e) {
       if (e?.response?.status === 404) {
           getError("Не верно указан город")
       } else if (e?.response?.status === 401) {
            getError("Не верно указан токен")
        } else {
           getError(e?.message)
       }
    }
}

const initCli = async () => {
    const args = getArgsHelpers(process.argv)
    if (args.h) {
        getHelp()
    }else if (args.s) {
        setKeyValue(STORAGE_KEYS.city, args.s)
    } else if (args.t) {
        await setKeyValue(STORAGE_KEYS.token, args.t)
    } else {
        getForCast()
    }
}

initCli()



