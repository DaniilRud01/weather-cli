const getArgsHelpers = (args) => {
    const res = {}
    const [first, second, ...other] = args
    other.forEach((arg, idx, array) => {
        if (arg.charAt(0) === "-") {
            if (idx === array.length - 1) {
                res[arg.substring(1)] = true
            } else if (array[idx + 1].charAt(0) !== "-") {
                res[arg.substring(1)] = array[idx + 1]
            } else {
                res[arg.substring(1)] = true;
            }
        }
    })
    return res
}

export { getArgsHelpers }