import chalk from "chalk";
import dedent from "dedent"

const getError = (error) => {
    console.log(`${chalk.bgRed("Error")} ${error}`)
}

const getSuccess = (message) => {
    console.log(`${chalk.bgGreen("SUCCESS")} ${message}`)
}

const printWeather = (weather) => {
    console.log(dedent(`
        ${chalk.bgYellowBright(` Погода в городе: ${weather.name} `)}
        Погода: ${weather.weather[0].description}
        Температура: ${weather.main.temp}
        Ощущается как: ${weather.main.feels_like}
        Влажность: ${weather.main.humidity}%
    `))
}

const getHelp = () => {
    console.log(dedent(`
        ${chalk.bgCyan("HELP")}
        Без параметров вывод погоды
        -s [CITY]: установка города
        -h вывод помощи
        -t [API_KEY] для сохранения токена
    `))
}

export { getError, getSuccess, getHelp, printWeather }