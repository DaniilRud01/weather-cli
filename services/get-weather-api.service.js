import {readKeyValue, STORAGE_KEYS} from "./storage.service.js";
import axios from "axios"

const getWeather = async () => {
    const token = await readKeyValue(STORAGE_KEYS.token)
    const city = await readKeyValue(STORAGE_KEYS.city)

    const { data } = await axios("https://api.openweathermap.org/data/2.5/weather", {
        params: {
            q: city,
            "appid": token,
            "lang": "ru",
            "units": "metric"
        }
    })
    return data;
}

export { getWeather }