import {homedir} from "os"
import {join} from "path"
import {promises} from "fs"

const STORAGE_KEYS = {
    token: "token",
    city: "city"
}


const filePath = join(homedir(), "weather-data.json")

const saveKeyValue = async (key, value) => {
    let data = {}
    data[key] = value

    if (await isExistFile(filePath)) {
        const readExistFile = await promises.readFile(filePath)
        data = {
            ...JSON.parse(readExistFile),
            [key]: value,
        }
    }
    await promises.writeFile(filePath, JSON.stringify(data))
}

const readKeyValue = async (key) => {
    if (await isExistFile(filePath)) {
        const file = await promises.readFile(filePath)
        const parseFile = JSON.parse(file)
        return parseFile[key]
    }
    return null
}

const isExistFile = async (path) => {
    try {
        await promises.stat(path)
        return true
    } catch (e) {
        return false
    }
}

export {saveKeyValue, readKeyValue, STORAGE_KEYS}